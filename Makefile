dc_up:
	docker-compose up -d

dc_build:
	docker-compose build --no-cache

dc_rebuild:
	docker-compose up -d --build

dc_down:
	docker-compose down --remove-orphans

dc_install:
	make dc_build
	docker-compose run --rm app composer install && npm install && php artisan key:generate && npm run build


dc_prod_up:
	docker-compose -f docker-compose-prod.yml up -d

dc_prod_down:
	docker-compose -f docker-compose-prod.yml down --remove-orphans

dc_prod_build:
	docker-compose -f docker-compose-prod.yml build --no-cache

dc_prod_rebuild:
	docker-compose -f docker-compose-prod.yml up -d --build


dc_push:
	docker build . -f docker/prod/app/Dockerfile -t registry.gitlab.com/savagedays/sqlka-fullstack/app --pull
	docker push registry.gitlab.com/savagedays/sqlka-fullstack/app


dc_app:
	docker-compose exec app bash

dc_mariadb:
	docker-compose exec mariadb bash

dc_redis:
	docker-compose exec redis bash


dc_prod_app:
	docker-compose -f docker-compose-prod.yml exec app bash


dc_migrate:
	docker-compose exec app php artisan migrate

dc_migrate_fresh:
	docker-compose exec app php artisan migrate:fresh


dc_test:
	docker-compose exec app php artisan test

dc_test_coverage:
	docker-compose exec app php vendor/bin/phpunit --coverage-html public/tests


dc_deptrac_scheme:
	docker-compose exec app php vendor/bin/deptrac analyze --config-file=deptrac-layers.yaml --formatter=graphviz-image --output=./deptrac-scheme.png

dc_deptrac:
	docker-compose exec app php vendor/bin/deptrac analyze --config-file=deptrac-layers.yaml
	docker-compose exec app php vendor/bin/deptrac analyze --config-file=deptrac-modules.yaml


dc_pint:
	docker-compose exec app php vendor/bin/pint --config=pint.config.json

dc_pint_list:
	docker-compose exec app php vendor/bin/pint --config pint.config.json --test -v
