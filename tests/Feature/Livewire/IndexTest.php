<?php

namespace Tests\Feature\Livewire;

use App\Modules\Question\Adapter\Http\Livewire\Questions;
use Livewire\Livewire;
use Tests\TestCase;

/**
 * @coversNothing
 */
class IndexTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(Questions::class);

        $component->assertStatus(200);
    }
}
