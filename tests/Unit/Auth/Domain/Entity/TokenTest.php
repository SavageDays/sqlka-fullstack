<?php

namespace Tests\Unit\Auth\Domain\Entity;

use App\Modules\Auth\Domain\Entity\Token;
use App\Modules\Auth\Domain\Service\EncryptionServiceInterface;
use DomainException;
use PHPUnit\Framework\TestCase;

/** @covers \App\Modules\Auth\Domain\Entity\Token */
class TokenTest extends TestCase
{
    private EncryptionServiceInterface $encryptionService;

    private string $userId;
    private string $payloadForUserId;
    private string $secondPayloadForUserId;
    private string $anotherUserId;
    private string $payloadForAnotherUserId;

    protected function setUp(): void
    {
        parent::setUp();

        $this->encryptionService = $this->createMock(EncryptionServiceInterface::class);

        $this->userId = '01GTNMW98K8TN8E4DAA9HWHM11';
        $this->payloadForUserId = 'eyJpdiI6InBrMURBcGJ4aW1xbTNlMVdBZmRaMHc9PSIsInZhbHVlIjoidm5GclV4b3p6US9xRWJCbG4vRmFXdlRWaWhUVm1wdXcrUkhyak8wUlFxRThoSE9STFhKc3dndzlsM1Y4VGd5MyIsIm1hYyI6Ijc0ZTJlMWU3YWE1MjI0ZWY4MTIwNmYwYjI3OWNjNTBhODNhM2YxM2UyMGRmMWZmNjZjZmYxOWUwMDkxN2RmMmIiLCJ0YWciOiIifQ==';
        $this->secondPayloadForUserId = 'eyJpdiI6Ii92b1duMk9KUzNkd3FBUjQ2KzdiK0E9PSIsInZhbHVlIjoidnNoK2ErdEZwVzBnNTJOTmh2c05IVkpYdmQzWVFqem94NzN5RkpiTEhlNGdOeklkT0JFMTVLMkYvUW5xak9pcyIsIm1hYyI6ImUyMjY1MjMzOTYzNDU1M2Q5NDEyNjJiZDVkOTBiMjU0ZGRjY2I3YjUzNGQ3MTZkYzgzZjg5YjMzZmFhNGZmNjkiLCJ0YWciOiIifQ==';

        $this->anotherUserId = '01GTNN1EHKMBDPJ87WB2MWVBF5';
        $this->payloadForAnotherUserId = 'eyJpdiI6IkZOaW51RnYweU80MFJIKzdzRlZlZHc9PSIsInZhbHVlIjoiRXNKclNJYmN2RnZKZm9qUWVjOWdCTnA1Q1RVaTIydThoVHVZU2p1QXpaZDk3bENtbE9zTnQ3RXNpdTBJOFJTciIsIm1hYyI6IjAwYTU4MjRkY2VkZmIxZjU5Yjk4OWRlYjNmNDJhNWFhYjU1MzM5ZGEwOTNhZWQ1MjU1YmY5MWVlMzE2NGJhY2EiLCJ0YWciOiIifQ==';
    }

    public function test_creating(): void
    {
        $this->encryptionService
            ->expects(
                $this->any(),
            )
            ->method('encrypt')
            ->with($this->userId)
            ->willReturn($this->payloadForUserId);

        $payload = $this->encryptionService->encrypt($this->userId);

        $token = new Token($this->userId, $payload);

        $this->assertEquals(
            $this->userId,
            $token->getUserId(),
        );
        $this->assertEquals(
            $payload,
            $token->getToken(),
        );

        $this->assertNotEquals(
            $this->anotherUserId,
            $token->getUserId(),
        );
        $this->assertNotEquals(
            $this->payloadForAnotherUserId,
            $token->getToken(),
        );
    }

    public function test_correct_update(): void
    {
        $token = new Token($this->userId, $this->payloadForUserId);

        $this->encryptionService
            ->expects(
                $this->any()
            )
            ->method('decrypt')
            ->with($this->secondPayloadForUserId)
            ->willReturn($this->userId);

        $token->update(
            $this->encryptionService,
            $this->secondPayloadForUserId,
        );

        $this->assertNotEquals(
            $this->payloadForUserId,
            $token->getToken(),
        );
    }

    public function test_uncorrect_update(): void
    {
        $token = new Token($this->userId, $this->payloadForUserId);

        $this->encryptionService
            ->expects(
                $this->any()
            )
            ->method('decrypt')
            ->with($this->payloadForAnotherUserId)
            ->willReturn($this->anotherUserId);

        $this->expectException(DomainException::class);

        $token->update(
            $this->encryptionService,
            $this->payloadForAnotherUserId,
        );
    }
}
