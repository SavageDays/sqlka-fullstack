<?php

namespace Auth\Domain\Entity;

use App\Modules\Auth\Domain\Entity\User;
use App\Modules\Auth\Domain\Service\IdServiceInterface;
use App\Modules\Auth\Infrastructure\Service\IdService;
use Carbon\CarbonImmutable;
use DomainException;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Modules\Auth\Domain\Entity\User
 */
class UserTest extends TestCase
{
    private readonly Generator $faker;
    private readonly IdServiceInterface $idService;

    public function __construct(string $name)
    {
        parent::__construct($name);

        $this->faker = Factory::create();
        $this->idService = app(IdService::class);
    }

    public function test_default_user_creation(): void
    {
        $id = $this->idService->generate();
        $userName = $this->faker->userName();

        $currentTime = CarbonImmutable::now()
            ->format('Y-m-d H:i:s');

        $user = new User($id, $userName);

        $userCreationTime = CarbonImmutable::parse(
            $user->getCreatedAt(),
        );

        $this->assertEquals($id, $user->getId());
        $this->assertEquals($userName, $user->getName());
        $this->assertTrue(
            $userCreationTime->gte($currentTime),
        );
        $this->assertNull($user->getActivatedAt());
    }

    public function test_user_created_at_field(): void
    {
        $id = $this->idService->generate();
        $userName = $this->faker->userName();
        $createdAt = CarbonImmutable::now()
            ->subMinute();

        $user = new User(
            $id,
            $userName,
            $createdAt,
        );

        $this->assertEquals(
            $createdAt->format('Y-m-d H:i:s'),
            $user->getCreatedAt()->format('Y-m-d H:i:s'),
        );
    }

    public function test_user_activated_at_field(): void
    {
        $id = $this->idService->generate();
        $userName = $this->faker->userName();
        $createdAt = CarbonImmutable::now()
            ->subHour();
        $activatedAt = CarbonImmutable::now()
            ->subMinute();

        $user = new User(
            $id,
            $userName,
            $createdAt,
            $activatedAt,
        );

        $this->assertNotNull(
            $user->getActivatedAt(),
        );
        $this->assertEquals(
            $activatedAt->format('Y-m-d H:i:s'),
            $user->getActivatedAt()->format('Y-m-d H:i:s'),
        );
        $this->assertTrue(
            $user->isActive(),
        );
    }

    public function test_activate_user(): void
    {
        $id = $this->idService->generate();
        $userName = $this->faker->userName();

        $user = new User($id, $userName);

        $this->assertFalse(
            $user->isActive(),
        );

        $user->activate();

        $this->assertTrue(
            $user->isActive(),
        );
    }

    public function test_double_activation_user(): void
    {
        $id = $this->idService->generate();
        $userName = $this->faker->userName();

        $user = new User($id, $userName);

        $user->activate();

        $this->assertTrue(
            $user->isActive(),
        );

        $this->expectException(DomainException::class);

        $user->activate();
    }
}
