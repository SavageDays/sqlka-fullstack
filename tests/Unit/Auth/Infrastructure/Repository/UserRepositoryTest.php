<?php

namespace Tests\Unit\Auth\Infrastructure\Repository;

use App\Modules\Auth\Domain\Entity\User;
use App\Modules\Auth\Domain\Repository\UserRepositoryInterface;
use App\Modules\Auth\Domain\Service\IdServiceInterface;
use App\Modules\Auth\Infrastructure\Repository\UserRepository;
use App\Modules\Auth\Infrastructure\Service\IdService;
use DomainException;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * @covers \App\Modules\Auth\Infrastructure\Repository\UserRepository
 */
class UserRepositoryTest extends TestCase
{
    use RefreshDatabase;

    private readonly UserRepositoryInterface $userRepository;
    private readonly IdServiceInterface $idService;
    private readonly Generator $faker;

    public function __construct(string $name)
    {
        parent::__construct($name);

        $this->userRepository = app(UserRepository::class);
        $this->idService = app(IdService::class);

        $this->faker = Factory::create();
    }

    public function test_add_find_and_update_user(): void
    {
        $id = $this->idService->generate();
        $userName = $this->faker->userName();
        $user = new User($id, $userName);
        $createdAt = $user->getCreatedAt()->format('Y-m-d H:i:s');
        $activatedAt = $user->getActivatedAt()?->format('Y-m-d H:i:s');

        DB::shouldReceive('table')
            ->with(
                $this->userRepository->getTableName()
            )
            ->once()
            ->andReturnSelf();
        DB::shouldReceive('insert')
            ->once()
            ->with([
                'id' => $user->getId(),
                'name' => $user->getName(),
                'created_at' => $createdAt,
                'activated_at' => $activatedAt,
            ])
            ->andReturnTrue();

        $this->userRepository->add($user);

        $fetchedUser = new stdClass();
        $fetchedUser->id = $id;
        $fetchedUser->name = $userName;
        $fetchedUser->created_at = $createdAt;
        $fetchedUser->activated_at = $activatedAt;

        DB::shouldReceive('table')
            ->with(
                $this->userRepository->getTableName()
            )
            ->once()
            ->andReturnSelf();
        DB::shouldReceive('where')
            ->with('id', $user->getId())
            ->once()
            ->andReturnSelf();
        DB::shouldReceive('first')
            ->once()
            ->andReturn($fetchedUser);

        $selectedUser = $this->userRepository->find($id);

        $this->assertEquals(
            $user->getId(),
            $selectedUser->getId(),
        );
        $this->assertEquals(
            $user->getName(),
            $selectedUser->getName(),
        );
        $this->assertEquals(
            $user->getCreatedAt()->format('Y-m-d H:i:s'),
            $selectedUser->getCreatedAt()->format('Y-m-d H:i:s'),
        );
        $this->assertEquals(
            $user->getActivatedAt()?->format('Y-m-d H:i:s'),
            $selectedUser->getActivatedAt()?->format('Y-m-d H:i:s'),
        );

        $selectedUser->activate();

        DB::shouldReceive('table')
            ->with(
                $this->userRepository->getTableName()
            )
            ->once()
            ->andReturnSelf();
        DB::shouldReceive('where')
            ->with('id', $selectedUser->getId())
            ->once()
            ->andReturnSelf();
        DB::shouldReceive('update')
            ->with([
                'activated_at' => $selectedUser->getActivatedAt()?->format('Y-m-d H:i:s'),
            ])
            ->once()
            ->andReturn(1);

        $this->userRepository->edit($selectedUser);
    }

    public function test_get_user(): void
    {
        $id = $this->idService->generate();

        DB::shouldReceive('table')
            ->with(
                $this->userRepository->getTableName()
            )
            ->once()
            ->andReturnSelf();
        DB::shouldReceive('where')
            ->with('id', $id)
            ->once()
            ->andReturnSelf();
        DB::shouldReceive('first')
            ->once()
            ->andReturnNull();

        $this->expectException(DomainException::class);

        $this->userRepository->get($id);
    }
}
