import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/theme.bundle.css',
                'resources/css/theme-dark.bundle.css',
                'resources/css/github.min.css',
                'resources/css/app.css',
                'resources/js/app.js',
                'resources/js/bootstrap.bundle.min.js',
                'resources/js/theme.bundle.js',
            ],
            refresh: true,
        }),
    ],
});
