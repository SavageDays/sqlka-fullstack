<div>
    <div class="card shadow-lg mb-0">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <h2 class="mb-0">Текст вопроса</h2>
                <span class="badge bg-light">↩</span>
            </div>
            <div class="form-group mb-0">
                <input type="text" id="questionText" wire:model.lazy="searchText" wire:keydown.enter="load()" class="form-control form-control-flush form-control-auto" placeholder="Вывести список..." style="overflow: hidden; overflow-wrap: break-word; height: 70px;">
            </div>
        </div>
    </div>

    <div class="d-flex justify-content-center">
        <div class="avatar my-5 my-md-6">
            <span wire:loading.class="bg-primary-soft text-primary rotated" wire:loading.class.remove="bg-light text-dark bg-success-soft text-success"
                  wire:target="load" class="avatar-title fs-lg rounded-circle {{ $status === 2 ? 'bg-success-soft text-success' : 'bg-light text-dark' }}">
                <i wire:loading class="fe fe-aperture"></i>
                @switch($status)
                    @case(0)
                        <i wire:loading.remove class="fe fe-search"></i>
                        @break
                    @case(2)
                        <i class="fe fe-check-circle"></i>
                        @break
                @endswitch
            </span>
        </div>
    </div>

    @foreach($questions as $question)
        <div class="card">
            <div class="card-header h-auto py-5">
                <h3 class="card-header-title">
                    {{ $question->text }}
                </h3>
                <button class="badge rounded-pill bg-light border-0 ms-4" onclick="this.classList.toggle('rotate-180')" type="button" data-bs-toggle="collapse" href="#answers{{ $question->id }}" role="button" aria-expanded="false" aria-controls="answers{{ $question->id }}">v</button>
            </div>
            <div class="card-body">
                <div class="list-group list-group-flush list-group-activity my-n3 collapse" id="answers{{ $question->id }}">
                    @foreach($question->answers as $answer)
                        <div class="list-group-item">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="avatar avatar-sm">
                                        <button title="Скопировать" class="avatar-title fs-lg bg-primary-soft rounded-circle text-primary border-0"
                                                onclick="document.getElementById('answer{{ $answer->id }}').select();document.execCommand('copy');this.classList.add('bg-success-soft','text-success')"
                                                data-bs-container="body" data-bs-toggle="popover" data-bs-placement="top" data-bs-content="Скопировано!">
                                            <i class="fe fe-copy"></i>
                                        </button>
                                    </div>

                                </div>
                                <div class="col overflow-hidden ms-n2">
                                    <h3 class="mb-3">
                                        <pre class="overflow-hidden mb-0"><code class="h4 language-sql">{{ $answer->text }}</code></pre>
                                    </h3>
                                    <textarea style="opacity: 0.01; height: 0; position: absolute; z-index: -1;" id="answer{{ $answer->id }}">{{ $answer->text }}</textarea>
                                    <small class="text-muted">
                                        <i class="fe fe-star"></i> {{ $answer->rating }}
                                    </small>
                                    @if(!$loop->last)
                                        <hr>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>
@push('scripts')
    @if(!is_null($token))
        <script>
            window.top.postMessage("{{ $token }}", '*');
            document.getElementById('questionText').focus();
        </script>
    @endif
@endpush
