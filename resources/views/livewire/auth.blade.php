@extends('welcome')
@section('content')
    <div class="row justify-content-center">
        <div class="col-12 col-md-5 col-xl-4 my-5">
            <h1 class="display-4 text-center mb-5">
                Войти
            </h1>
            <p class="text-muted text-center mb-5">
                {{ config('app.auth_description_text') }}
            </p>
            <form method="POST" action="/activate">
                <div class="form-group">
                    <label class="form-label">
                        Токен доступа
                    </label>
                    <input type="password" name="token" class="form-control mb-2" placeholder="eyJpdiI6IjJ3S0ZITT...">
                    @error('token')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button class="btn btn-lg w-100 btn-primary mb-3">
                    Войти
                </button>
                <div class="text-center">
                    <small class="text-muted text-center">
                        <a target="_blank" href="{{ config('app.admin_contact_link') }}">Нет токена?</a>
                    </small>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        window.top.postMessage("", '*');
        document.getElementById('questionText').focus();
    </script>
@endpush
