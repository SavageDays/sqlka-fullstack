@extends('welcome')
@section('content')
    @livewire('questions')
@endsection
@prepend('scripts')
    <script>
        document.addEventListener('token-updated', function (event) {
            window.top.postMessage(event.detail.token, '*');
            document.getElementById('questionText').focus();
            document.querySelectorAll('.language-sql').forEach((el) => {
                hljs.highlightBlock(el);
            });
        })
    </script>
@endprepend
