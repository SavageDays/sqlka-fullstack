<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        @livewireStyles
        @vite(['resources/css/theme.bundle.css', 'resources/css/github.min.css', 'resources/css/app.css'])
    </head>
    <body class="mh-100">
        <div class="container my-5 pb-5">
            @yield('content')
        </div>

        @livewireScripts
        @vite(['resources/js/bootstrap.bundle.min.js', 'resources/js/theme.bundle.js'])

        @stack('scripts')
    </body>
</html>
