<?php

declare(strict_types=1);

namespace App\Providers;

use App\Modules\Auth\Adapter\Internal\AuthAdapter;
use App\Modules\Auth\Adapter\Internal\AuthAdapterInterface;
use App\Modules\Auth\Application\Job\DispatcherInterface;
use App\Modules\Auth\Application\Service\UserService;
use App\Modules\Auth\Application\ServiceApi\UserServiceInterface;
use App\Modules\Auth\Domain\Repository\TokenRepositoryInterface;
use App\Modules\Auth\Domain\Repository\UserRepositoryInterface;
use App\Modules\Auth\Domain\Service\EncryptionServiceInterface;
use App\Modules\Auth\Domain\Service\IdServiceInterface;
use App\Modules\Auth\Infrastructure\Job\Dispatcher;
use App\Modules\Auth\Infrastructure\Repository\RedisTokenRepository;
use App\Modules\Auth\Infrastructure\Repository\UserRepository;
use App\Modules\Auth\Infrastructure\Service\EncryptionService;
use App\Modules\Auth\Infrastructure\Service\IdService;
use App\Modules\Question\Application\Service\QuestionService;
use App\Modules\Question\Application\ServiceApi\QuestionServiceInterface;
use App\Modules\Question\Domain\Repository\AnswerRepositoryInterface;
use App\Modules\Question\Domain\Repository\QuestionRepositoryInterface;
use App\Modules\Question\Infrastructure\Repository\AnswerRepostitory;
use App\Modules\Question\Infrastructure\Repository\QuestionRepository;
use Illuminate\Support\ServiceProvider;

class DIProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(
            UserRepositoryInterface::class,
            UserRepository::class,
        );

        $this->app->singleton(
            TokenRepositoryInterface::class,
            RedisTokenRepository::class,
        );

        $this->app->singleton(
            EncryptionServiceInterface::class,
            EncryptionService::class,
        );

        $this->app->singleton(
            UserServiceInterface::class,
            UserService::class,
        );

        $this->app->singleton(
            IdServiceInterface::class,
            IdService::class,
        );

        $this->app->singleton(
            DispatcherInterface::class,
            Dispatcher::class,
        );

        $this->app->singleton(
            QuestionRepositoryInterface::class,
            QuestionRepository::class,
        );

        $this->app->singleton(
            AnswerRepositoryInterface::class,
            AnswerRepostitory::class,
        );

        $this->app->singleton(
            QuestionServiceInterface::class,
            QuestionService::class,
        );

        $this->app->singleton(
            \App\Modules\Question\Domain\Service\IdServiceInterface::class,
            \App\Modules\Question\Infrastructure\Service\IdService::class,
        );

        $this->app->singleton(
            \App\Modules\Question\Application\Job\DispatcherInterface::class,
            \App\Modules\Question\Infrastructure\Job\Dispatcher::class,
        );

        $this->app->singleton(
            AuthAdapterInterface::class,
            AuthAdapter::class,
        );
    }

    public function boot(): void
    {
    }
}
