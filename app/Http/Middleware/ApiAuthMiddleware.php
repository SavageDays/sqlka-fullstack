<?php

namespace App\Http\Middleware;

use App\Modules\Auth\Application\ServiceApi\UserServiceInterface;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Throwable;

class ApiAuthMiddleware
{
    private const TOKEN_HEADER = 'X-AUTH-TOKEN';

    public function __construct(
        private readonly UserServiceInterface $userService,
    ) {
    }

    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->header(self::TOKEN_HEADER, '');

        try {
            $this->userService->getUserByToken($token);
        } catch (Throwable) {
            throw new UnauthorizedHttpException('', 'Указан неверный токен.');
        }

        /** @var Response $response */
        $response = $next($request);

        return $this->addTokenToResponse(
            $response,
            $token,
        );
    }

    private function addTokenToResponse(
        Response $response,
        string $currentToken,
    ): Response {
        $token = $this->userService->resetUserToken($currentToken);

        $response->headers->add([
            self::TOKEN_HEADER => $token,
        ]);

        return $response;
    }
}
