<?php

declare(strict_types=1);

namespace App\Modules\Auth\Infrastructure\Repository;

use App\Modules\Auth\Domain\Entity\User;
use App\Modules\Auth\Domain\Repository\UserRepositoryInterface;
use Carbon\CarbonImmutable;
use DomainException;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{
    private const TABLE_NAME = 'auth_users';

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function list(
        int $limit = 50,
        int $offset = 0
    ): array {
        $usersData = DB::table(self::TABLE_NAME)
            ->offset($offset)
            ->take($limit)
            ->get();

        return array_map(
            static function ($userData) {
                return new User(
                    $userData->id,
                    $userData->name,
                    CarbonImmutable::parse($userData->created_at),
                    $userData->activated_at
                        ? CarbonImmutable::parse($userData->activated_at)
                        : null,
                );
            },
            $usersData->toArray(),
        );
    }

    public function find(string $userId): ?User
    {
        $userData = DB::table(self::TABLE_NAME)
            ->where('id', $userId)
            ->first();

        if (is_null($userData)) {
            return null;
        }

        return new User(
            $userData->id,
            $userData->name,
            CarbonImmutable::parse($userData->created_at),
            $userData->activated_at
                ? CarbonImmutable::parse($userData->activated_at)
                : null,
        );
    }

    public function get(string $userId): User
    {
        $user = $this->find($userId);

        if (is_null($user)) {
            throw new DomainException("Пользователь $userId не найден.");
        }

        return $user;
    }

    public function add(User $user): void
    {
        DB::table(self::TABLE_NAME)
            ->insert([
                'id' => $user->getId(),
                'name' => $user->getName(),
                'created_at' => $user->getCreatedAt()->format('Y-m-d H:i:s'),
                'activated_at' => $user->getActivatedAt()?->format('Y-m-d H:i:s'),
            ]);
    }

    public function edit(User $user): void
    {
        DB::table(self::TABLE_NAME)
            ->where('id', $user->getId())
            ->update([
                'activated_at' => $user->getActivatedAt()?->format('Y-m-d H:i:s'),
            ]);
    }
}
