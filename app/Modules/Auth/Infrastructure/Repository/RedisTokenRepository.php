<?php

declare(strict_types=1);

namespace App\Modules\Auth\Infrastructure\Repository;

use App\Modules\Auth\Domain\Entity\Token;
use App\Modules\Auth\Domain\Repository\TokenRepositoryInterface;
use Illuminate\Support\Facades\Redis;

class RedisTokenRepository implements TokenRepositoryInterface
{
    private const TOKEN_PREFIX = 'auth_user_token_';

    public function save(Token $token): void
    {
        Redis::set(
            self::TOKEN_PREFIX.$token->getUserId(),
            $token->getToken(),
        );
    }

    public function get(string $userId): Token
    {
        $payload = Redis::get(
            self::TOKEN_PREFIX.$userId,
        );

        return new Token(
            $userId,
            $payload,
        );
    }

    public function isActual(Token $token): bool
    {
        $actualToken = $this->get(
            $token->getUserId(),
        );

        return $actualToken->getToken() === $token->getToken();
    }
}
