<?php

declare(strict_types=1);

namespace App\Modules\Auth\Infrastructure\Service;

use App\Modules\Auth\Domain\Service\EncryptionServiceInterface;
use Illuminate\Contracts\Encryption\Encrypter;

readonly class EncryptionService implements EncryptionServiceInterface
{
    public function __construct(
        private Encrypter $encrypter,
    ) {
    }

    public function encrypt(string $string): string
    {
        return $this->encrypter->encrypt($string);
    }

    public function decrypt(string $encryptedString): string
    {
        return $this->encrypter->decrypt($encryptedString);
    }
}
