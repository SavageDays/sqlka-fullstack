<?php

declare(strict_types=1);

namespace App\Modules\Auth\Infrastructure\Service;

use App\Modules\Auth\Domain\Service\IdServiceInterface;
use Symfony\Component\Uid\Ulid;

class IdService implements IdServiceInterface
{
    public function generate(): string
    {
        return Ulid::generate();
    }
}
