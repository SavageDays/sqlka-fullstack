<?php

declare(strict_types=1);

namespace App\Modules\Auth\Adapter\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserActivationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'token' => 'required|string',
        ];
    }
}
