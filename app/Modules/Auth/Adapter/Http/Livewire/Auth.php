<?php

namespace App\Modules\Auth\Adapter\Http\Livewire;

use Livewire\Component;

class Auth extends Component
{
    public function render()
    {
        return view('livewire.auth');
    }
}
