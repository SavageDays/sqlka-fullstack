<?php

declare(strict_types=1);

namespace App\Modules\Auth\Adapter\Http\Web;

use App\Modules\Auth\Adapter\Http\Requests\UserActivationRequest;
use App\Modules\Auth\Application\ServiceApi\UserServiceInterface;
use Illuminate\Routing\Controller;
use Throwable;

class UserActivationController extends Controller
{
    public function __construct(
        private readonly UserServiceInterface $userService,
    ) {
    }

    public function __invoke(UserActivationRequest $request)
    {
        $token = $request->validated('token');

        try {
            $newToken = $this->userService->resetUserToken($token);
        } catch (Throwable) {
            return redirect()->back()->withErrors([
                'token' => 'Указан неверный токен.',
            ]);
        }

        return redirect('/?token='.$newToken);
    }
}
