<?php

declare(strict_types=1);

namespace App\Modules\Auth\Adapter\Http\Web;

use Illuminate\Routing\Controller;

class AuthIndexController extends Controller
{
    public function __invoke()
    {
        return view('livewire.auth');
    }
}
