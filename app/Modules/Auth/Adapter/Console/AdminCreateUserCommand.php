<?php

declare(strict_types=1);

namespace App\Modules\Auth\Adapter\Console;

use App\Modules\Auth\Application\Dto\Input\UserCreationDto;
use App\Modules\Auth\Application\ServiceApi\UserServiceInterface;

class AdminCreateUserCommand extends AbstractUserCommand
{
    private const COMMAND_NAME = 'admin-create-user';
    private const COMMAND_DESCRIPTION = 'Создание и активация нового пользователя';

    public function __construct(
        private readonly UserServiceInterface $userService,
    ) {
        parent::__construct();
    }

    protected function getCommandName(): string
    {
        return self::COMMAND_NAME;
    }

    protected function getCommandParams(): string
    {
        return '{userName : Имя пользователя}';
    }

    protected function getCommandDescription(): string
    {
        return self::COMMAND_DESCRIPTION;
    }

    public function handle(): int
    {
        $userName = $this->input->getArgument('userName');

        $userCreationDto = new UserCreationDto($userName);
        $userId = $this->userService->createUser($userCreationDto);

        $token = $this->userService->activateUser($userId);

        $this->output->write($token, true);

        return self::SUCCESS;
    }
}
