<?php

declare(strict_types=1);

namespace App\Modules\Auth\Adapter\Console;

use App\Modules\Auth\Application\Dto\Input\GetUsersDto;
use App\Modules\Auth\Application\ServiceApi\UserServiceInterface;

class GetUsersCommand extends AbstractUserCommand
{
    private const COMMAND_NAME = 'get-users';
    private const COMMAND_DESCRIPTION = 'Вывод списка пользователей';

    public function __construct(
        private readonly UserServiceInterface $userService,
    ) {
        parent::__construct();
    }

    protected function getCommandName(): string
    {
        return self::COMMAND_NAME;
    }

    protected function getCommandParams(): string
    {
        return '{--offset=0 : Смещение от начала} {--limit=30 : Количество записей}';
    }

    protected function getCommandDescription(): string
    {
        return self::COMMAND_DESCRIPTION;
    }

    public function handle(): int
    {
        $offset = (int) $this->option('offset');
        $limit = (int) $this->option('limit');

        $getUsersDto = new GetUsersDto(
            $limit,
            $offset,
        );
        $users = $this->userService->getUsers($getUsersDto);

        $this->output->table(
            [
                'id',
                'name',
                'created_at',
                'activated_at',
            ],
            $users,
        );

        return self::SUCCESS;
    }
}
