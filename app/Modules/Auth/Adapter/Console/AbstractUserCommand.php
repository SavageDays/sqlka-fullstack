<?php

declare(strict_types=1);

namespace App\Modules\Auth\Adapter\Console;

use Illuminate\Console\Command;

abstract class AbstractUserCommand extends Command
{
    private const SIGNATURE_PREFIX = 'auth-user';

    public function __construct()
    {
        $this->configurate();

        parent::__construct();
    }

    abstract public function handle(): int;

    abstract protected function getCommandName(): string;

    abstract protected function getCommandDescription(): string;

    abstract protected function getCommandParams(): string;

    private function configurate(): void
    {
        $this->setCommandSignature();
        $this->setCommandDescription();
    }

    private function setCommandSignature(): void
    {
        $this->signature = self::SIGNATURE_PREFIX.':'
            .$this->getCommandName().' '
            .$this->getCommandParams();
    }

    private function setCommandDescription(): void
    {
        $this->description = $this->getCommandDescription();
    }
}
