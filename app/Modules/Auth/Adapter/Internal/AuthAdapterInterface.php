<?php

declare(strict_types=1);

namespace App\Modules\Auth\Adapter\Internal;

use App\Modules\Auth\Application\Dto\Output\UserDto;

interface AuthAdapterInterface
{
    public function getUserByToken(string $payload): UserDto;

    public function updateUserToken(string $payload): string;
}
