<?php

declare(strict_types=1);

namespace App\Modules\Auth\Adapter\Internal;

use App\Modules\Auth\Application\Dto\Output\UserDto;
use App\Modules\Auth\Application\ServiceApi\UserServiceInterface;

readonly class AuthAdapter implements AuthAdapterInterface
{
    public function __construct(
        private UserServiceInterface $userService,
    ) {
    }

    public function getUserByToken(string $payload): UserDto
    {
        return $this->userService->getUserByToken($payload);
    }

    public function updateUserToken(string $payload): string
    {
        return $this->userService->resetUserToken($payload);
    }
}
