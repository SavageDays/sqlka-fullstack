<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Job;

use App\Modules\Auth\Application\Dto\Input\UserCreationDto;
use App\Modules\Auth\Domain\Entity\User;
use App\Modules\Auth\Domain\Repository\UserRepositoryInterface;

readonly class UserCreationJob implements JobInterface
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private UserCreationDto $userCreationDto,
    ) {
    }

    public function handle(): void
    {
        $user = new User(
            $this->userCreationDto->userId,
            $this->userCreationDto->userName,
        );

        $this->userRepository->add($user);
    }
}
