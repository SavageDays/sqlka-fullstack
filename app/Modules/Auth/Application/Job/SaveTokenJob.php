<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Job;

use App\Modules\Auth\Domain\Entity\Token;
use App\Modules\Auth\Domain\Repository\TokenRepositoryInterface;
use App\Modules\Auth\Domain\Service\EncryptionServiceInterface;

readonly class SaveTokenJob implements JobInterface
{
    public function __construct(
        private EncryptionServiceInterface $encryptionService,
        private TokenRepositoryInterface $tokenRepository,
        private string $userId,
    ) {
    }

    public function handle(): void
    {
        $payload = $this->encryptionService->encrypt(
            $this->userId,
        );

        $token = new Token(
            $this->userId,
            $payload,
        );

        $this->tokenRepository->save($token);
    }
}
