<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Job;

interface DispatcherInterface
{
    public function dispatch(JobInterface $job): void;

    public function dispatchNow(JobInterface $job): void;
}
