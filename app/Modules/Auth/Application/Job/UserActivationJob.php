<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Job;

use App\Modules\Auth\Domain\Repository\UserRepositoryInterface;

readonly class UserActivationJob implements JobInterface
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private string $userId,
    ) {
    }

    public function handle(): void
    {
        $user = $this->userRepository->get(
            $this->userId,
        );

        $user->activate();

        $this->userRepository->edit($user);
    }
}
