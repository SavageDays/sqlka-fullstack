<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Job;

interface JobInterface
{
    public function handle(): void;
}
