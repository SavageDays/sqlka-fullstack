<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\ServiceApi;

use App\Modules\Auth\Application\Dto\Input\GetUsersDto;
use App\Modules\Auth\Application\Dto\Input\UserCreationDto;
use App\Modules\Auth\Application\Dto\Output\UserDto;

interface UserServiceInterface
{
    /** @return string User id */
    public function createUser(UserCreationDto $userCreationDto): string;

    /** @return string Token payload */
    public function activateUser(string $userId): string;

    /** @return UserDto[] */
    public function getUsers(GetUsersDto $getUsersDto): array;

    /** @return string Token payload */
    public function resetUserToken(string $token): string;

    public function getUserByToken(string $payload): UserDto;
}
