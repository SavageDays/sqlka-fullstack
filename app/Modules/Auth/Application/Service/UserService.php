<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Service;

use App\Modules\Auth\Application\Dto\Input\GetUsersDto;
use App\Modules\Auth\Application\Dto\Input\UserCreationDto;
use App\Modules\Auth\Application\Dto\Output\UserDto;
use App\Modules\Auth\Application\Job\DispatcherInterface;
use App\Modules\Auth\Application\Job\SaveTokenJob;
use App\Modules\Auth\Application\Job\UserActivationJob;
use App\Modules\Auth\Application\Job\UserCreationJob;
use App\Modules\Auth\Application\ServiceApi\UserServiceInterface;
use App\Modules\Auth\Domain\Entity\Token;
use App\Modules\Auth\Domain\Entity\User;
use App\Modules\Auth\Domain\Repository\TokenRepositoryInterface;
use App\Modules\Auth\Domain\Repository\UserRepositoryInterface;
use App\Modules\Auth\Domain\Service\EncryptionServiceInterface;
use App\Modules\Auth\Domain\Service\IdServiceInterface;
use DomainException;

readonly class UserService implements UserServiceInterface
{
    public function __construct(
        private DispatcherInterface $dispatcher,
        private UserRepositoryInterface $userRepository,
        private TokenRepositoryInterface $tokenRepository,
        private EncryptionServiceInterface $encryptionService,
        private IdServiceInterface $idService,
    ) {
    }

    public function createUser(UserCreationDto $userCreationDto): string
    {
        $userId = $userCreationDto->userId
            ?: $this->idService->generate();
        $userCreationDto = new UserCreationDto(
            $userCreationDto->userName,
            $userId,
        );

        $job = new UserCreationJob(
            $this->userRepository,
            $userCreationDto,
        );
        $this->dispatcher->dispatchNow($job);

        return $userId;
    }

    public function activateUser(string $userId): string
    {
        if (!$user = $this->userRepository->find($userId)) {
            throw new DomainException('Пользователь с указанным идентификатором не найден.');
        }

        $job = new UserActivationJob(
            $this->userRepository,
            $user->getId(),
        );
        $this->dispatcher->dispatchNow($job);

        return $this->updateUserToken($user);
    }

    public function resetUserToken(string $token): string
    {
        $userId = $this->encryptionService->decrypt($token);

        if (!$user = $this->userRepository->find($userId)) {
            throw new DomainException('Пользователь с указанным идентификатором не найден.');
        }

        if (!$user->isActive()) {
            throw new DomainException('Пользователь не активирован.');
        }

        $token = new Token(
            $user->getId(),
            $token,
        );

        if (
            !$this->tokenRepository->isActual($token)
        ) {
            throw new DomainException('Указан некорректный токен.');
        }

        return $this->updateUserToken($user);
    }

    public function getUsers(GetUsersDto $getUsersDto): array
    {
        $users = $this->userRepository->list(
            $getUsersDto->limit,
            $getUsersDto->offset,
        );

        return array_map(
            static fn ($user) => [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'createdAt' => $user->getCreatedAt(),
                'activatedAt' => $user->getActivatedAt(),
            ],
            $users,
        );
    }

    public function getUserByToken(string $payload): UserDto
    {
        $userId = $this->encryptionService->decrypt($payload);

        $user = $this->userRepository->get($userId);

        if (!$user->isActive()) {
            throw new DomainException('Пользователь не активирован.');
        }

        $token = $this->getUserToken($user);

        if (
            !$this->tokenRepository->isActual($token)
        ) {
            throw new DomainException('Указан некорректный токен.');
        }

        return new UserDto(
            $user->getId(),
            $user->getName(),
            $user->getCreatedAt(),
            $user->getActivatedAt(),
        );
    }

    private function updateUserToken(User $user): string
    {
        $this->saveUserToken($user);

        $token = $this->getUserToken($user);

        return $token->getToken();
    }

    private function saveUserToken(User $user): void
    {
        $job = new SaveTokenJob(
            $this->encryptionService,
            $this->tokenRepository,
            $user->getId(),
        );
        $this->dispatcher->dispatchNow($job);
    }

    private function getUserToken(User $userId): Token
    {
        return $this->tokenRepository->get(
            $userId->getId(),
        );
    }
}
