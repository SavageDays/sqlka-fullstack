<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Dto\Input;

final readonly class GetUsersDto
{
    public function __construct(
        public int $limit = 50,
        public int $offset = 0,
    ) {
    }
}
