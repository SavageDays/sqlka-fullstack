<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Dto\Input;

final readonly class UserCreationDto
{
    public function __construct(
        public string $userName,
        public ?string $userId = null,
    ) {
    }
}
