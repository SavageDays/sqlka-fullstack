<?php

declare(strict_types=1);

namespace App\Modules\Auth\Application\Dto\Output;

use DateTimeInterface;

final readonly class UserDto
{
    public function __construct(
        public string $id,
        public string $name,
        public DateTimeInterface $createdAt,
        public ?DateTimeInterface $activatedAt,
    ) {
    }
}
