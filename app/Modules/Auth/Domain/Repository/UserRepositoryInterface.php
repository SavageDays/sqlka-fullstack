<?php

declare(strict_types=1);

namespace App\Modules\Auth\Domain\Repository;

use App\Modules\Auth\Domain\Entity\User;

interface UserRepositoryInterface
{
    /** @return User[] */
    public function list(
        int $limit = 50,
        int $offset = 0,
    ): array;

    public function get(string $userId): User;

    public function find(string $userId): ?User;

    public function add(User $user): void;

    public function edit(User $user): void;

    public function getTableName(): string;
}
