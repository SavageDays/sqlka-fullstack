<?php

declare(strict_types=1);

namespace App\Modules\Auth\Domain\Repository;

use App\Modules\Auth\Domain\Entity\Token;

interface TokenRepositoryInterface
{
    public function save(Token $token): void;

    public function get(string $userId): Token;

    public function isActual(Token $token): bool;
}
