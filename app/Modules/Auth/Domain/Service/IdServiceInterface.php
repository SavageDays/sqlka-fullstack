<?php

declare(strict_types=1);

namespace App\Modules\Auth\Domain\Service;

interface IdServiceInterface
{
    public function generate(): string;
}
