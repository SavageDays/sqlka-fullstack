<?php

declare(strict_types=1);

namespace App\Modules\Auth\Domain\Service;

interface EncryptionServiceInterface
{
    public function encrypt(string $string): string;

    public function decrypt(string $encryptedString): string;
}
