<?php

declare(strict_types=1);

namespace App\Modules\Auth\Domain\Entity;

use App\Modules\Auth\Domain\Service\EncryptionServiceInterface;
use DomainException;

class Token
{
    public function __construct(
        private readonly string $userId,
        private string $payload,
    ) {
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getToken(): string
    {
        return $this->payload;
    }

    public function update(
        EncryptionServiceInterface $encryptionService,
        string $payload
    ): void {
        $isValid = $this->isValidPayloadForUser(
            $encryptionService,
            $payload,
        );

        if (!$isValid) {
            throw new DomainException('Payload токена не подходит для текущего пользователя.');
        }

        $this->payload = $payload;
    }

    private function isValidPayloadForUser(
        EncryptionServiceInterface $encryptionService,
        string $payload
    ): bool {
        $userId = $encryptionService->decrypt($payload);

        return $userId === $this->userId;
    }
}
