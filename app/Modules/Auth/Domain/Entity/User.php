<?php

declare(strict_types=1);

namespace App\Modules\Auth\Domain\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use DomainException;

class User
{
    private readonly string $id;
    private readonly string $name;
    private readonly DateTimeInterface $createdAt;
    private ?DateTimeInterface $activatedAt;

    public function __construct(
        string $id,
        string $name,
        ?DateTimeInterface $createdAt = null,
        ?DateTimeInterface $activatedAt = null,
    ) {
        $this->id = $id;
        $this->name = $name;

        $this->createdAt = $createdAt
            ?: new DateTimeImmutable();

        $this->activatedAt = $activatedAt
            ?: null;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getActivatedAt(): ?DateTimeInterface
    {
        return $this->activatedAt;
    }

    public function isActive(): bool
    {
        return !is_null($this->activatedAt);
    }

    public function activate(): void
    {
        if (!is_null($this->activatedAt)) {
            throw new DomainException('Пользователь уже активирован.');
        }

        $this->activatedAt = new DateTimeImmutable();
    }
}
