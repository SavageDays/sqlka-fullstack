<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\ServiceApi;

use App\Modules\Question\Application\Dto\Input\AddAnswerDto;
use App\Modules\Question\Application\Dto\Input\FindQuestionsDto;
use App\Modules\Question\Application\Dto\Output\QuestionDto;

interface QuestionServiceInterface
{
    /**
     * @return QuestionDto[]
     */
    public function findQuestions(FindQuestionsDto $findQuestionsDto): array;

    public function addAnswer(AddAnswerDto $answerCreationDto): void;
}
