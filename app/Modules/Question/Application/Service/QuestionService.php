<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Service;

use App\Modules\Question\Application\Dto\Input\AddAnswerDto;
use App\Modules\Question\Application\Dto\Input\AnswerEdittingDto;
use App\Modules\Question\Application\Dto\Input\CreateAnswerDto;
use App\Modules\Question\Application\Dto\Input\FindQuestionsDto;
use App\Modules\Question\Application\Dto\Input\QuestionCreationDto;
use App\Modules\Question\Application\Dto\Output\AnswerDto;
use App\Modules\Question\Application\Dto\Output\QuestionDto;
use App\Modules\Question\Application\Job\AddAnswerJob;
use App\Modules\Question\Application\Job\AddQuestionJob;
use App\Modules\Question\Application\Job\DispatcherInterface;
use App\Modules\Question\Application\Job\EditAnswerJob;
use App\Modules\Question\Application\ServiceApi\QuestionServiceInterface;
use App\Modules\Question\Domain\Repository\AnswerRepositoryInterface;
use App\Modules\Question\Domain\Repository\QuestionRepositoryInterface;
use App\Modules\Question\Domain\Service\IdServiceInterface;

readonly class QuestionService implements QuestionServiceInterface
{
    public function __construct(
        private QuestionRepositoryInterface $questionRepository,
        private AnswerRepositoryInterface $answerRepository,
        private IdServiceInterface $idService,
        private DispatcherInterface $dispatcher,
    ) {
    }

    public function findQuestions(FindQuestionsDto $findQuestionsDto): array
    {
        $questions = $this->questionRepository->list(
            $findQuestionsDto->text,
            $findQuestionsDto->limit,
        );

        return array_map(
            static fn ($question) => new QuestionDto(
                $question->getId(),
                $question->getText(),
                array_map(
                    static fn ($answer) => new AnswerDto(
                        $answer->getId(),
                        $answer->getUserId(),
                        $answer->getText(),
                        $answer->getRating(),
                    ),
                    $question->getAnswers(),
                ),
            ),
            $questions
        );
    }

    public function addAnswer(AddAnswerDto $answerCreationDto): void
    {
        $question = $this->questionRepository->findByText(
            $answerCreationDto->questionText,
        );

        if (is_null($question)) {
            $questionId = $this->createQuestion(
                $answerCreationDto->questionText,
            );

            $question = $this->questionRepository->get($questionId);
        }

        $answer = $question->findUserAnswer(
            $answerCreationDto->userId,
        );

        if (is_null($answer)) {
            $this->createAnswer(
                $question->getId(),
                $answerCreationDto->userId,
                $answerCreationDto->answerText,
            );

            return;
        }

        $this->editAnswer(
            $answer->getId(),
            $answerCreationDto->answerText,
        );
    }

    private function createAnswer(
        string $questionId,
        string $userId,
        string $text,
    ): void {
        $id = $this->idService->generate();

        $createAnswerDto = new CreateAnswerDto(
            $id,
            $questionId,
            $userId,
            $text,
        );

        $job = new AddAnswerJob(
            $this->answerRepository,
            $createAnswerDto,
        );
        $this->dispatcher->dispatchNow($job);
    }

    public function editAnswer(
        string $id,
        string $text,
    ): void {
        $answerEdittingDto = new AnswerEdittingDto(
            $id,
            $text,
        );

        $job = new EditAnswerJob(
            $this->answerRepository,
            $answerEdittingDto,
        );
        $this->dispatcher->dispatchNow($job);
    }

    private function createQuestion(string $text): string
    {
        $id = $this->idService->generate();

        $questionCreationDto = new QuestionCreationDto(
            $id,
            $text,
        );
        $job = new AddQuestionJob(
            $this->questionRepository,
            $questionCreationDto,
        );
        $this->dispatcher->dispatchNow($job);

        return $id;
    }
}
