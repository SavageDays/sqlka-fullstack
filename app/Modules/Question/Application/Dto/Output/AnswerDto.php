<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Dto\Output;

final readonly class AnswerDto
{
    public function __construct(
        public string $id,
        public string $userId,
        public string $text,
        public int $rating,
    ) {
    }
}
