<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Dto\Input;

final readonly class AddAnswerDto
{
    public function __construct(
        public string $userId,
        public string $questionText,
        public string $answerText,
    ) {
    }
}
