<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Dto\Input;

final readonly class QuestionCreationDto
{
    public function __construct(
        public string $id,
        public string $text,
    ) {
    }
}
