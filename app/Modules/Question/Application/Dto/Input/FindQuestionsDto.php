<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Dto\Input;

final readonly class FindQuestionsDto
{
    public function __construct(
        public string $text = '',
        public int $limit = 10,
    ) {
    }
}
