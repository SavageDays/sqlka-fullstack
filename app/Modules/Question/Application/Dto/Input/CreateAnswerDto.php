<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Dto\Input;

final readonly class CreateAnswerDto
{
    public function __construct(
        public string $answerId,
        public string $questionId,
        public string $userId,
        public string $text,
    ) {
    }
}
