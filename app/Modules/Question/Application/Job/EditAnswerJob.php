<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Job;

use App\Modules\Question\Application\Dto\Input\AnswerEdittingDto;
use App\Modules\Question\Domain\Repository\AnswerRepositoryInterface;

readonly class EditAnswerJob implements JobInterface
{
    public function __construct(
        private AnswerRepositoryInterface $answerRepository,
        private AnswerEdittingDto $answerEdittingDto,
    ) {
    }

    public function handle(): void
    {
        $answer = $this->answerRepository->get(
            $this->answerEdittingDto->id,
        );

        $answer->updateText(
            $this->answerEdittingDto->text,
        );

        $this->answerRepository->edit($answer);
    }
}
