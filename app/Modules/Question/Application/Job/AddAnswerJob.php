<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Job;

use App\Modules\Question\Application\Dto\Input\CreateAnswerDto;
use App\Modules\Question\Domain\Entity\Answer;
use App\Modules\Question\Domain\Repository\AnswerRepositoryInterface;

readonly class AddAnswerJob implements JobInterface
{
    public function __construct(
        private AnswerRepositoryInterface $answerRepository,
        private CreateAnswerDto $createAnswerDto,
    ) {
    }

    public function handle(): void
    {
        $answer = new Answer(
            $this->createAnswerDto->answerId,
            $this->createAnswerDto->userId,
            $this->createAnswerDto->text,
        );

        $this->answerRepository->add(
            $answer,
            $this->createAnswerDto->questionId,
        );
    }
}
