<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Job;

use App\Modules\Question\Application\Dto\Input\QuestionCreationDto;
use App\Modules\Question\Domain\Entity\Question;
use App\Modules\Question\Domain\Repository\QuestionRepositoryInterface;

readonly class AddQuestionJob implements JobInterface
{
    public function __construct(
        private QuestionRepositoryInterface $questionRepository,
        private QuestionCreationDto $questionCreationDto,
    ) {
    }

    public function handle(): void
    {
        $question = new Question(
            $this->questionCreationDto->id,
            $this->questionCreationDto->text,
            [],
        );

        $this->questionRepository->add($question);
    }
}
