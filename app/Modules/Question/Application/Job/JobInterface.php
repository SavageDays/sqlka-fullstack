<?php

declare(strict_types=1);

namespace App\Modules\Question\Application\Job;

interface JobInterface
{
    public function handle(): void;
}
