<?php

declare(strict_types=1);

namespace App\Modules\Question\Adapter\Http\Web;

use App\Modules\Auth\Adapter\Internal\AuthAdapterInterface;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Throwable;

class GetQuestionsController extends Controller
{
    public function __construct(
        private readonly AuthAdapterInterface $userService,
    ) {
    }

    public function __invoke(Request $request)
    {
        $token = $request->input('token', '');

        try {
            $this->userService->getUserByToken($token);
        } catch (Throwable) {
            return redirect('/auth');
        }

        return view('app');
    }
}
