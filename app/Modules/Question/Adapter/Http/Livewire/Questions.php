<?php

namespace App\Modules\Question\Adapter\Http\Livewire;

use App\Modules\Auth\Adapter\Internal\AuthAdapterInterface;
use App\Modules\Question\Application\Dto\Input\FindQuestionsDto;
use App\Modules\Question\Application\ServiceApi\QuestionServiceInterface;
use Livewire\Component;

class Questions extends Component
{
    private AuthAdapterInterface $authAdapter;
    private QuestionServiceInterface $questionService;

    protected $queryString = ['token'];

    public $questions = [];
    public $searchText = '';
    public $status = 0;
    public $token = '';

    public function render()
    {
        if (empty($this->searchText)) {
            return view('livewire.questions');
        }

        $this->updateToken();

        $findQuestionsDto = new FindQuestionsDto(
            $this->searchText,
        );
        $this->questions = $this->questionService->findQuestions($findQuestionsDto);

        $this->changeStatus();

        return view('livewire.questions');
    }

    private function updateToken()
    {
        try {
            $this->token = $this->authAdapter->updateUserToken(
                $this->token,
            );
        } catch (\Throwable) {
            return redirect('/auth');
        }

        $this->dispatchBrowserEvent(
            'token-updated',
            ['token' => $this->token],
        );
    }

    public function changeStatus()
    {
        if (1 === $this->status) {
            $this->status = 2;

            return;
        }

        $this->status = 2;
    }

    public function boot(
        AuthAdapterInterface $authAdapter,
        QuestionServiceInterface $questionService,
    ) {
        $this->authAdapter = $authAdapter;
        $this->questionService = $questionService;
    }

    public function load()
    {
    }
}
