<?php

declare(strict_types=1);

namespace App\Modules\Question\Adapter\Http\Api;

use App\Modules\Auth\Adapter\Internal\AuthAdapterInterface;
use App\Modules\Auth\Application\Dto\Output\UserDto;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Throwable;

abstract class AuthApiController extends Controller
{
    public function __construct(
        private readonly AuthAdapterInterface $userService,
    ) {
    }

    protected function getUser(Request $request): UserDto
    {
        $token = $request->header('X-AUTH-TOKEN', '');

        try {
            return $this->userService->getUserByToken($token);
        } catch (Throwable) {
            throw new BadRequestHttpException('Указан неверный токен.');
        }
    }
}
