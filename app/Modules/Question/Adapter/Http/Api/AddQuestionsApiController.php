<?php

declare(strict_types=1);

namespace App\Modules\Question\Adapter\Http\Api;

use App\Modules\Auth\Adapter\Internal\AuthAdapterInterface;
use App\Modules\Question\Application\Dto\Input\AddAnswerDto;
use App\Modules\Question\Application\ServiceApi\QuestionServiceInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AddQuestionsApiController extends AuthApiController
{
    public function __construct(
        private readonly QuestionServiceInterface $questionService,
        AuthAdapterInterface $userService,
    ) {
        parent::__construct($userService);
    }

    public function __invoke(Request $request)
    {
        $user = $this->getUser($request);
        $question = $request->input('question', '');
        $answer = $request->input('answer', '');

        if (empty($question) || empty($answer)) {
            return new BadRequestHttpException('Переданы пустые параметры вопроса.');
        }

        $answerCreationDto = new AddAnswerDto(
            $user->id,
            $question,
            $answer,
        );
        $this->questionService->addAnswer($answerCreationDto);

        return response(null, 201);
    }
}
