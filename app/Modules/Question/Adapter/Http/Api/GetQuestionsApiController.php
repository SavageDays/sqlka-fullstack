<?php

declare(strict_types=1);

namespace App\Modules\Question\Adapter\Http\Api;

use App\Modules\Auth\Adapter\Internal\AuthAdapterInterface;
use App\Modules\Question\Application\Dto\Input\FindQuestionsDto;
use App\Modules\Question\Application\ServiceApi\QuestionServiceInterface;
use Illuminate\Http\Request;

class GetQuestionsApiController extends AuthApiController
{
    public function __construct(
        private readonly QuestionServiceInterface $questionService,
        AuthAdapterInterface $userService,
    ) {
        parent::__construct($userService);
    }

    public function __invoke(Request $request)
    {
        $text = $request->input('text', '');

        $findQuestionsDto = new FindQuestionsDto($text);
        $questions = $this->questionService->findQuestions($findQuestionsDto);

        return response()->json([
            'questions' => $questions,
        ]);
    }
}
