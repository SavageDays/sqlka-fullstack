<?php

declare(strict_types=1);

namespace App\Modules\Question\Infrastructure\Repository;

use App\Modules\Question\Domain\Entity\Question;
use App\Modules\Question\Domain\Repository\AnswerRepositoryInterface;
use App\Modules\Question\Domain\Repository\QuestionRepositoryInterface;
use Carbon\CarbonImmutable;
use DomainException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class QuestionRepository implements QuestionRepositoryInterface
{
    private const TABLE_NAME = 'question_questions';

    public function __construct(
        private readonly AnswerRepositoryInterface $answerRepository,
    ) {
    }

    public function list(
        string $text = '',
        int $limit = 10,
    ): array {
        $tableName = $this->getTableName();
        $answerTableName = $this->answerRepository->getTableName();

        $questionsData = DB::table(
            $this->getTableName(),
            'q',
        )
            ->fromSub(
                static function (Builder $query) use ($text, $tableName, $answerTableName) {
                    return $query->selectSub(
                        static function (Builder $subQuery) use ($answerTableName) {
                            return $subQuery->selectRaw('count(a.id)')
                                ->from($answerTableName, 'a')
                                ->whereColumn('a.question_id', 'q2.id')
                                ->groupBy('a.question_id');
                        },
                        'cnt',
                    )
                        ->addSelect('q2.*')
                        ->from(
                            $tableName,
                            'q2',
                        )
                        ->where(
                            'q2.text',
                            'like',
                            '%'.$text.'%',
                        );
                },
                'q',
            )
            ->orderByDesc('cnt')
            ->take($limit)
            ->get()
            ->toArray();

        $questionIds = array_column($questionsData, 'id');

        $answersData = $this->answerRepository->getByQuestionIds($questionIds);

        $questions = [];

        foreach ($questionsData as $questionData) {
            $question = new Question(
                $questionData->id,
                $questionData->text,
                $answersData[$questionData->id] ?? [],
                CarbonImmutable::parse($questionData->created_at),
            );

            $questions[] = $question;
        }

        return $questions;
    }

    public function findByText(string $text): ?Question
    {
        return $this->findBy('text', $text);
    }

    public function get(string $id): Question
    {
        $question = $this->findBy('id', $id);

        if (is_null($question)) {
            throw new DomainException('Вопрос не найден.');
        }

        return $question;
    }

    private function findBy(
        string $column,
        string $value
    ): ?Question {
        $questionData = DB::table(
            $this->getTableName(),
        )
            ->where($column, $value)
            ->first();

        if (is_null($questionData)) {
            return null;
        }

        $answerData = $this->answerRepository->getByQuestionIds([
            $questionData->id,
        ]);

        return new Question(
            $questionData->id,
            $questionData->text,
            $answerData[$questionData->id] ?? [],
            CarbonImmutable::parse($questionData->created_at),
        );
    }

    public function add(Question $question): void
    {
        DB::table(
            $this->getTableName(),
        )
            ->insert([
                'id' => $question->getId(),
                'text' => $question->getText(),
                'created_at' => $question->getCreatedAt()
                    ->format('Y-m-d H:i:s'),
            ]);

        foreach ($question->getAnswers() as $answer) {
            $this->answerRepository->add(
                $answer,
                $question->getId()
            );
        }
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }
}
