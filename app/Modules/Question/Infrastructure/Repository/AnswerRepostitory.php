<?php

declare(strict_types=1);

namespace App\Modules\Question\Infrastructure\Repository;

use App\Modules\Question\Domain\Entity\Answer;
use App\Modules\Question\Domain\Repository\AnswerRepositoryInterface;
use Carbon\CarbonImmutable;
use DomainException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class AnswerRepostitory implements AnswerRepositoryInterface
{
    private const TABLE_NAME = 'question_answers';

    public function getByQuestionIds(array $questionIds): array
    {
        $answersData = DB::table(
            $this->getTableName(),
        )
            ->selectRaw('first_value(id) over (partition by text) id, '.
                'first_value(user_id) over (partition by text) user_id, '.
                'first_value(created_at) over (partition by text) created_at, '.
                'count(user_id) as rating')
            ->addSelect(['question_id', 'text'])
            ->whereIn('question_id', $questionIds)
            ->groupBy(['question_id', 'text'])
            ->orderByDesc('rating')
            ->get();

        $answers = [];

        foreach ($answersData as $answerData) {
            $answer = new Answer(
                $answerData->id,
                $answerData->user_id,
                $answerData->text,
                $answerData->rating,
                CarbonImmutable::parse($answerData->created_at),
            );

            $answers[$answerData->question_id][] = $answer;
        }

        return $answers;
    }

    public function get(string $id): Answer
    {
        $tableName = $this->getTableName();

        $answersData = DB::table(
            $tableName,
            'a',
        )
            ->addSelect('*')
            ->selectSub(
                static function (Builder $query) use ($tableName) {
                    $query->selectRaw('count(user_id)')
                        ->from($tableName)
                        ->whereColumn('question_id', 'a.question_id')
                        ->whereColumn('text', 'a.text')
                        ->groupBy('text');
                },
                'rating'
            )
            ->where('id', $id)
            ->orderByDesc('rating')
            ->first();

        if (is_null($answersData)) {
            throw new DomainException('Ответ не найден.');
        }

        return new Answer(
            $answersData->id,
            $answersData->user_id,
            $answersData->text,
            $answersData->rating,
            CarbonImmutable::parse($answersData->created_at),
        );
    }

    public function add(
        Answer $answer,
        string $questionId,
    ): void {
        DB::table(
            $this->getTableName(),
        )
            ->insert([
                'id' => $answer->getId(),
                'question_id' => $questionId,
                'user_id' => $answer->getUserId(),
                'text' => $answer->getText(),
                'created_at' => $answer->getCreatedAt(),
            ]);
    }

    public function edit(Answer $answer): void
    {
        DB::table(
            $this->getTableName(),
        )
            ->where('id', $answer->getId())
            ->update([
                'text' => $answer->getText(),
            ]);
    }

    public function getTableName(): string
    {
        return self::TABLE_NAME;
    }
}
