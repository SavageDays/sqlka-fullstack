<?php

declare(strict_types=1);

namespace App\Modules\Question\Infrastructure\Service;

use App\Modules\Question\Domain\Service\IdServiceInterface;
use Symfony\Component\Uid\Ulid;

class IdService implements IdServiceInterface
{
    public function generate(): string
    {
        return Ulid::generate();
    }
}
