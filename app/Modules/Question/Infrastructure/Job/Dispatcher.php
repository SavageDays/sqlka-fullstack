<?php

declare(strict_types=1);

namespace App\Modules\Question\Infrastructure\Job;

use App\Modules\Question\Application\Job\DispatcherInterface;
use App\Modules\Question\Application\Job\JobInterface;
use Illuminate\Contracts\Bus\Dispatcher as JobDispatcher;

readonly class Dispatcher implements DispatcherInterface
{
    public function __construct(
        private JobDispatcher $dispatcher,
    ) {
    }

    public function dispatch(JobInterface $job): void
    {
        $this->dispatcher->dispatch($job);
    }

    public function dispatchNow(JobInterface $job): void
    {
        $this->dispatcher->dispatchNow($job);
    }
}
