<?php

declare(strict_types=1);

namespace App\Modules\Question\Domain\Service;

interface IdServiceInterface
{
    public function generate(): string;
}
