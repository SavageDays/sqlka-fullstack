<?php

declare(strict_types=1);

namespace App\Modules\Question\Domain\Entity;

use DateTimeImmutable;
use DateTimeInterface;

class Question
{
    private readonly string $id;
    private readonly string $text;
    /** @var Answer[] */
    private array $answers;
    private readonly DateTimeInterface $createdAt;

    /** @param Answer[] $answers */
    public function __construct(
        string $id,
        string $text,
        array $answers,
        ?DateTimeInterface $createdAt = null,
    ) {
        $this->id = $id;
        $this->text = $text;
        $this->answers = $answers;
        $this->createdAt = $createdAt
            ?: new DateTimeImmutable();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getText(): string
    {
        return $this->text;
    }

    /** @return Answer[] */
    public function getAnswers(): array
    {
        return $this->answers;
    }

    public function findUserAnswer(string $userId): ?Answer
    {
        foreach ($this->answers as $answer) {
            if ($answer->getUserId() === $userId) {
                return $answer;
            }
        }

        return null;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}
