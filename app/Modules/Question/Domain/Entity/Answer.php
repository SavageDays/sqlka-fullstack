<?php

declare(strict_types=1);

namespace App\Modules\Question\Domain\Entity;

use DateTimeImmutable;
use DateTimeInterface;

class Answer
{
    private readonly string $id;
    private readonly string $userId;
    private string $text;
    private readonly int $rating;
    private readonly DateTimeInterface $createdAt;

    public function __construct(
        string $id,
        string $userId,
        string $text,
        int $rating = 0,
        ?DateTimeInterface $createdAt = null,
    ) {
        $this->id = $id;
        $this->userId = $userId;
        $this->text = $text;
        $this->rating = $rating;
        $this->createdAt = $createdAt
            ?: new DateTimeImmutable();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function updateText(string $text): void
    {
        $this->text = $text;
    }

    public function getRating(): int
    {
        return $this->rating < 1
            ? 0
            : $this->rating - 1;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}
