<?php

declare(strict_types=1);

namespace App\Modules\Question\Domain\Repository;

use App\Modules\Question\Domain\Entity\Question;

interface QuestionRepositoryInterface
{
    /** @return Question[] */
    public function list(
        string $text = '',
        int $limit = 10,
    ): array;

    public function findByText(string $text): ?Question;

    public function get(string $id): Question;

    public function add(Question $question): void;

    public function getTableName(): string;
}
