<?php

declare(strict_types=1);

namespace App\Modules\Question\Domain\Repository;

use App\Modules\Question\Domain\Entity\Answer;

interface AnswerRepositoryInterface
{
    /**
     * @param string[] $questionIds
     *
     * @return Answer[][]
     */
    public function getByQuestionIds(array $questionIds): array;

    public function get(string $id): Answer;

    public function add(Answer $answer, string $questionId): void;

    public function edit(Answer $answer): void;

    public function getTableName(): string;
}
