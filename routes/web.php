<?php

use App\Modules\Auth\Adapter\Http\Web\AuthIndexController;
use App\Modules\Auth\Adapter\Http\Web\UserActivationController;
use App\Modules\Question\Adapter\Http\Web\GetQuestionsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/auth', AuthIndexController::class);
Route::post('/activate', UserActivationController::class);

Route::get('/', GetQuestionsController::class);
