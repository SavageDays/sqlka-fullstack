<?php

use App\Modules\Question\Adapter\Http\Api\AddQuestionsApiController;
use App\Modules\Question\Adapter\Http\Api\GetQuestionsApiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')
    ->group(
        static function () {
            Route::middleware('auth.token')
                ->group(
                    static function () {
                        Route::get('/questions', GetQuestionsApiController::class);
                    }
                );
            Route::post('/questions', AddQuestionsApiController::class);
        }
    );
